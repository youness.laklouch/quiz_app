package com.example.quiz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import  java.lang.* ;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

public class PlayersOperations {
    private DatabaseHelper dbHelper;
    private SQLiteDatabase dataBase;

    /*
     * Le constructeur
     */
    public PlayersOperations(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    /*
     * Ouverture  de la connexion à la base de données avec un accès en lecture et écriture
     */
    public void open() {
        dataBase = dbHelper.getWritableDatabase();
    }

    /*
     * Fermeture de la connexion à la base de données
     */
    public void close() {
        dbHelper.close();
    }

    /*
     * Ajout d'un player dans la base de données
     */
    public long ajouterPlayer(User p) {
        // A COMPLETER
        ContentValues values = new ContentValues();

        values.put(dbHelper.getNom(), p.getFirstName());
        values.put(dbHelper.getScore(), p.getScore());

        long contactId = dataBase.insert(dbHelper.getTableContact(), null, values);
        return contactId;
    }

    /*
     * Lister tous les contacts de la base de données
     */
    public Vector<User> listerTousLesPlayers() {
        // A COMPLETER
        String tabColonne[] = new String[3];
        Vector<User> l = new Vector<User>();
        tabColonne[0] = dbHelper.getId();
        tabColonne[1] = dbHelper.getNom();
        tabColonne[2] = dbHelper.getScore();

        Cursor c = dataBase.query(dbHelper.getTableContact(), tabColonne, null, null, null, null, null);

        int numeroColonneId = c.getColumnIndex(dbHelper.getId());
        int numeroColonneNom = c.getColumnIndex(dbHelper.getNom());
        int numeroColonneScore = c.getColumnIndex(dbHelper.getScore());

        if (c.moveToFirst() == true) {
            do {
                int id = c.getInt(numeroColonneId);
                String nom = c.getString(numeroColonneNom);
                int score = c.getInt(numeroColonneScore);
                User user = new User(id, nom, score);
                l.add(user);
            } while (c.moveToNext());
        }
        Collections.sort(l ,l.elementAt(3));
        Collections.reverse(l);
        return l;
    }
}
