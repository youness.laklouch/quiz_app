package com.example.quiz;


import java.util.Collections;
import java.util.List;

public class Question {
    private final String mQuestion;
    private final List<String> mChoiceList;
    private final int mAnswerIndex;
    private final String mUrl;

    public Question(String question, List<String> choiceList, int answerIndex, String url) {
        mQuestion = question;
        mChoiceList = choiceList;
        mAnswerIndex = answerIndex;
        mUrl = url ;
    }

    public String getText(){
        return mQuestion;
    }
    public String getUrl(){
        return mUrl;
    }

    public List<String> getAnswers(){
        return mChoiceList;
    }

    public int getAnswerIndex() {
        return mAnswerIndex;
    }
}
