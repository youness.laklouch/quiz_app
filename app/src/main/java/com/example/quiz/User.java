package com.example.quiz;

import java.util.Comparator;

public class User implements Comparator<User> {
    private String mFirstName;
    private int score;
    int id;
    public User ( int id , String mFirstName ,int score){
        this.id = id ;
        this.mFirstName = mFirstName;
        this.score =score;
    }
    public User ( String mFirstName, int score){
        this.score=score;
        this.mFirstName=mFirstName;
    }
    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public void setScore(int score) { this.score = score; }

    public int getScore (){
        return this.score;
    }

    @Override
    public String toString() {
        return id + " - "+ mFirstName + " : " + score ;
    }

    @Override
    public int compare(User user1, User user2) {
        return user1.score- user2.score;
    }

}