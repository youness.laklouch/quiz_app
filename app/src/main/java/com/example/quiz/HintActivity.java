package com.example.quiz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class HintActivity extends AppCompatActivity {
    Button Return;
    WebView webView;
    WebViewClient webViewClient ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hint);
        Return=findViewById(R.id.WebViewReturnButton);
        webView=(WebView)findViewById(R.id.WebView);
        Bundle b = getIntent().getExtras();
        String url = b.getString("url");
        webViewClient =new WebViewClient();
        webView.setWebViewClient(webViewClient);
        webView.loadUrl(url);
        Return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}