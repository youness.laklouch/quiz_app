package com.example.quiz;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.lang.*;

public class DatabaseHelper extends SQLiteOpenHelper {
    // Version de la base de données
    private static final int VERSION_BASE_DE_DONNEEES = 4;
    // Nom du fichier contenant la base de données
    private static final String NOM_BASE_DE_DONNEES = "players.db";
    // Nom de la table qui sera créée dans la base de données
    private static String TABLE_USER = "User";
    // Les 3 champs de la table
    private static final String ID = "id";
    private static final String NOM = "nom";
    private static final String SCORE = "score";
    // Requête SQL de création de la table "players" dans la base de données
    private static final String REQUETE_CREATION_TABLE = "CREATE TABLE " + TABLE_USER+ "(" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            NOM + " TEXT NOT NULL, " +
            SCORE + " TEXT NOT NULL);";



    // Requête SQL de suppression de la table "players" dans la base de données
    private static final String REQUETE_SUPPRESSION_TABLE = "DROP TABLE IF EXISTS " +
            TABLE_USER+ ";";

    /*
     * Le constructeur appelle le constructeur de la classe SQLiteOpenHelper en
     * passant en paramètre le nom de la base de données et le numéro de version
     *
     */
    public DatabaseHelper(Context context) {
        super(context, NOM_BASE_DE_DONNEES, null, VERSION_BASE_DE_DONNEEES);
    }

    /*
     * Création de la table "contacts" dans la base de données
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(REQUETE_CREATION_TABLE);
    }

    /*
     * Méthode appelée lorsque la base de données est mise à jour.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(REQUETE_SUPPRESSION_TABLE);
        onCreate(db);
    }

    /*
     * Renvoie le numéro de version de la base de données
     */
    public int getVersionBaseDeDonneees() {
        return VERSION_BASE_DE_DONNEEES;
    }

    /*
     * Renvoie le nom de la table
     */
    public String getTableContact() {
        return TABLE_USER;
    }

    /*
     * Renvoie le champ id de la table
     */
    public String getId() {
        return ID;
    }

    /*
     * Renvoie le champ nom de la table
     */
    public String getNom() {
        return NOM;
    }

    /*
     * Renvoie le champ numeroTelephone de la table
     */
    public String getScore() {
        return SCORE;
    }
}


