package com.example.quiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

public class LevelActivity extends AppCompatActivity {
    private RadioGroup Game , Difficulty , Category;
    private Button start;
    private Intent intent;
    private String cat="",diff="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        Game=(RadioGroup) findViewById(R.id.GameRadioGroup);
        Difficulty=(RadioGroup) findViewById(R.id.DifficultyRadioGroup);
        Category=(RadioGroup) findViewById(R.id.CategoryRadioGroup);
        start = findViewById(R.id.Start);
        for (int i = 0; i < Difficulty.getChildCount(); i++) {
            Difficulty.getChildAt(i).setEnabled(false);
        }
        for (int i = 0; i < Category.getChildCount(); i++) {
            Category.getChildAt(i).setEnabled(false);
        }
        Game.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                            @Override
                                            public void onCheckedChanged(RadioGroup radioGroup, int j) {
                                                int gamechecked = Game.getCheckedRadioButtonId();
                                                switch (gamechecked) {
                                                    case R.id.GameRadioButtonBD:
                                                        for (int i = 0; i < Difficulty.getChildCount(); i++) {
                                                            Difficulty.getChildAt(i).setEnabled(false);
                                                            Log.i("case2", "caseEnabled");
                                                        }
                                                        for (int i = 0; i < Category.getChildCount(); i++) {
                                                            Category.getChildAt(i).setEnabled(false);
                                                        }
                                                        intent = new Intent(getApplicationContext(), GameActivity.class);
                                                        break;

                                                    case R.id.GameRadioButtonJSON:
                                                        for (int i = 0; i < Difficulty.getChildCount(); i++) {
                                                            Difficulty.getChildAt(i).setEnabled(true);
                                                        }
                                                        for (int i = 0; i < Category.getChildCount(); i++) {
                                                            Category.getChildAt(i).setEnabled(true);
                                                        }
                                                        Difficulty.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                            @Override
                                                            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                                                                int difficulty = Difficulty.getCheckedRadioButtonId();
                                                                switch (difficulty) {
                                                                    case -1:
                                                                        diff = "";
                                                                        break;
                                                                    case R.id.DifficultyRadioButtonEasy:
                                                                        diff = "&difficulty=easy";
                                                                        break;
                                                                    case R.id.DifficultyRadioButtonMedium:
                                                                        diff = "&difficulty=Medium";
                                                                        break;
                                                                    case R.id.DifficultyRadioButtonHard:
                                                                        diff = "&difficulty=Hard";
                                                                        break;
                                                                }
                                                            }
                                                        });
                                                        Category.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                            @Override
                                                            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                                                                int category = Category.getCheckedRadioButtonId();
                                                                switch (category) {
                                                                    case -1:
                                                                        cat = "";
                                                                        break;
                                                                    case R.id.CategoryRadioButtonGeneralKnowledge:
                                                                        cat = "&category=9";
                                                                        break;
                                                                    case R.id.CategoryRadioButtonScienceNature:
                                                                        cat = "&category=17";
                                                                        break;
                                                                    case R.id.CategoryRadioButtonPolitics:
                                                                        cat = "&category=24";
                                                                        break;
                                                                }
                                                            }
                                                        });
                                                        intent = new Intent(getApplicationContext(), GameActivity2.class);
                                                        intent.putExtra("difficulty", diff);
                                                        intent.putExtra("category",cat);
                                                        break;

                                                }
                                            }

                                        }

        );

                start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(intent);
                    }
                });
            }
        }