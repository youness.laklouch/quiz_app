package com.example.quiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import java.util.Vector;
import android.widget.ListView;

public class ScoreActivity extends AppCompatActivity {
    ListView listView;
    Button returnButton;
    private PlayersOperations playerOperations ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        playerOperations = MainActivity.playerOperation ;
        playerOperations.open();
        Vector<User> lplayers = playerOperations.listerTousLesPlayers();
        playerOperations.close();
        listView =findViewById(R.id.listViewScore);
        returnButton = findViewById(R.id.buttonScoreReturn);
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, lplayers);
        listView.setAdapter(adapter);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext() , MainActivity.class);
                startActivity(i);
            }
        });

    }
}