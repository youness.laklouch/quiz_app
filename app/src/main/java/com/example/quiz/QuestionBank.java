package com.example.quiz;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class QuestionBank implements Serializable {
    private final List<Question> mQuestionList;
    private int size;
    private int mQuestionIndex=0;

    public QuestionBank(List<Question> questionList) {
        mQuestionList= questionList;
        Collections.shuffle(mQuestionList);
    }
    public Question getNextQuestion() {
        mQuestionIndex++;
        return getCurrentQuestion();
    }
    public Question getCurrentQuestion(){
        return mQuestionList.get(mQuestionIndex);
    }

    public int number() {
        size=mQuestionList.size();
        return size;
    }
}
