package com.example.quiz;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GameActivity2 extends AppCompatActivity implements  View.OnClickListener {

    private TextView questionText;
    private Button Answer1;
    private Button Answer2;
    private Button Answer3;
    private Button Answer4;

    private static final String sharedPrefUserInfo = "Shared_Pref_User_Info";
    private static final String sharedPrefUserInfoName = "Shared_pref_User_Info_NAme";
    private TextView T;
    String name, url, c_answer, question,difficulty,category;
    private int mRemainingQuestions = 4;

    private int Score = 0;
    static final String Bundle_extra_score = "Bundle_extra_score";
    CountDownTimer countDownTimer;
    PlayersOperations mPlayersOperations;
    TextView Hint;
    List<String> AnswerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game2);
        mPlayersOperations = MainActivity.playerOperation;
        questionText = findViewById(R.id.question);
        Answer1 = findViewById(R.id.Button1);
        Answer2 = findViewById(R.id.Button2);
        Answer3 = findViewById(R.id.Button3);
        Answer4 = findViewById(R.id.Button4);
        Answer1.setOnClickListener(this);
        Answer2.setOnClickListener(this);
        Answer3.setOnClickListener(this);
        Answer4.setOnClickListener(this);
        SharedPreferences sharedPreferences = getSharedPreferences(sharedPrefUserInfo, MODE_PRIVATE);
        name = sharedPreferences.getString(sharedPrefUserInfoName, "");
        url ="https://opentdb.com/api.php?amount=1%s%s&type=multiple";
        difficulty = getIntent().getExtras().getString("difficulty","");
        category = getIntent().getExtras().getString("category","");
        url=String.format(url, category, difficulty);
        Log.i("url",difficulty);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        Response.Listener<String> responseListner = new QuizResponseListner();
        Response.ErrorListener responseErrorListner = new QuizErrorResponseListener();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, responseListner, responseErrorListner);
        requestQueue.add(stringRequest);

        T = findViewById(R.id.Timer);
        countDownTimer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long l) {
                NumberFormat f = new DecimalFormat("00");
                long min = (l / 60000) % 60;
                long sec = (l / 1000) % 60;
                T.setText(f.format(min) + ":" + f.format(sec));
            }

            @Override
            public void onFinish() {
                if (mRemainingQuestions != 0) {
                    mRemainingQuestions--;
                    Toast.makeText(getApplicationContext(), "Time's up", Toast.LENGTH_SHORT).show();
                    requestQueue.add(stringRequest);
                    countDownTimer.start();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity2.this);
                    builder.setTitle("Well Done!")
                            .setMessage("Your score is " + Score)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                }
                            }).create().show();
                    countDownTimer.cancel();
                }
            }
        }.start();
    }

    private class QuizResponseListner implements Response.Listener<String> {
        @Override
        public void onResponse(String response) {
            JSONObject jsonObject = null;
            AnswerList = new ArrayList<>();
            try {
                jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONArray("results");
                JSONObject object = result.getJSONObject(0);
                question = object.getString("question");
                c_answer = object.getString("correct_answer");
                JSONArray f_answer = object.getJSONArray("incorrect_answers");
                questionText.setText(question);
                AnswerList.add(c_answer);
                AnswerList.add((String) f_answer.get(0));
                AnswerList.add((String) f_answer.get(1));
                AnswerList.add((String) f_answer.get(2));
                Collections.shuffle(AnswerList);
                Answer1.setText(AnswerList.get(0));
                Answer2.setText(AnswerList.get(1));
                Answer3.setText(AnswerList.get(2));
                Answer4.setText(AnswerList.get(3));

            } catch (JSONException e) {
                e.printStackTrace();
                questionText.setText("ERROR");
            }
        }
    }

    private class QuizErrorResponseListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            questionText.setText("échec de récupération de la question \n" + error.getLocalizedMessage());
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        countDownTimer.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();
    }

    @Override
    public void onClick(View view) {
        Button b = (Button) view;
        countDownTimer.start();
        mRemainingQuestions--;
        if (mRemainingQuestions == 0) {
            User mUser = new User(name, Score);
            mPlayersOperations.open();
            mPlayersOperations.ajouterPlayer(mUser);
            mPlayersOperations.close();
            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setTitle("Well Done!")
                    .setMessage("Your score is "+Score)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            Intent intent =new Intent(getApplicationContext(), ScoreActivity.class);
                            startActivity(intent);
                        }
                    }).create().show();
            countDownTimer.cancel();
        } else {
            if ((b.getText()).equals(c_answer)) {

                Score++;
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                Response.Listener<String> responseListner = new QuizResponseListner();
                Response.ErrorListener responseErrorListner = new QuizErrorResponseListener();
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url, responseListner, responseErrorListner);
                requestQueue.add(stringRequest);
            } else {
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                Response.Listener<String> responseListner = new QuizResponseListner();
                Response.ErrorListener responseErrorListner = new QuizErrorResponseListener();
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url, responseListner, responseErrorListner);
                requestQueue.add(stringRequest);
            }
        }
    }
}
