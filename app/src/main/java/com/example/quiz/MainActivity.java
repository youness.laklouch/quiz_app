package com.example.quiz;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.quiz.User;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    public static PlayersOperations playerOperation;
    private TextView textview;
    private EditText EditText;
    private Button PlayButton;
    String name;
    private final static int GameActivityNumber=43;
    private static final String sharedPrefUserInfo="Shared_Pref_User_Info";
    private static final String sharedPrefUserInfoName="Shared_pref_User_Info_NAme";
    private int score;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        playerOperation = new PlayersOperations(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textview=findViewById(R.id.mTextView);
        EditText=findViewById(R.id.mEditText);
        PlayButton=findViewById(R.id.mButton);
        PlayButton.setEnabled(false);
        Intent svc=new Intent(this, BackgroundSoundService.class);
        startService(svc);

        //String firstName = getSharedPreferences(sharedPrefUserInfo, MODE_PRIVATE).getString(sharedPrefUserInfoName, null);
        EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
            PlayButton.setEnabled(!editable.toString().isEmpty());
            name=editable.toString();
            }
        });
        PlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences= getSharedPreferences(sharedPrefUserInfo, MODE_PRIVATE);
                SharedPreferences.Editor editor=preferences.edit();
                editor.putString(sharedPrefUserInfoName, String.valueOf(EditText.getText()));
                editor.commit();
                //Intent gameActivityIntent =new Intent(getApplicationContext(), GameActivity.class);
                //startActivityForResult(gameActivityIntent, GameActivityNumber);
                //nbrPartie++;
                Intent i = new Intent(getApplicationContext(), LevelActivity.class);
                startActivity(i);
            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
