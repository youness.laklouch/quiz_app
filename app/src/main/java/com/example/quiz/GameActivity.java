package com.example.quiz;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quiz.Question;
import com.example.quiz.QuestionBank;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView questionText;
    private Button Answer1;
    private Button Answer2;
    private Button Answer3;
    private Button Answer4;

    private static final String sharedPrefUserInfo="Shared_Pref_User_Info";
    private static final String sharedPrefUserInfoName="Shared_pref_User_Info_NAme";
    private TextView T;
    String name, h;
    private int mRemainingQuestions;
    QuestionBank mQuestionBank = generateQuestions();
    Question mCurrentQuestion;
    private int Score =0;
    static final String Bundle_extra_score = "Bundle_extra_score";
    CountDownTimer countDownTimer;
    PlayersOperations mPlayersOperations;
    TextView Hint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        mPlayersOperations =MainActivity.playerOperation ;
        questionText = findViewById(R.id.question);
        Hint = findViewById(R.id.Hint);
        Answer1 = findViewById(R.id.Button1);
        Answer2 = findViewById(R.id.Button2);
        Answer3 = findViewById(R.id.Button3);
        Answer4 = findViewById(R.id.Button4);
        Answer1.setOnClickListener(this);
        Answer2.setOnClickListener(this);
        Answer3.setOnClickListener(this);
        Answer4.setOnClickListener(this);
        SharedPreferences sharedPreferences = getSharedPreferences(sharedPrefUserInfo, MODE_PRIVATE);
        name =sharedPreferences.getString(sharedPrefUserInfoName, "" );
        mCurrentQuestion = mQuestionBank.getCurrentQuestion();
        displayQuestion(mCurrentQuestion);
        mRemainingQuestions = mQuestionBank.number();
        T = findViewById(R.id.Timer);
        countDownTimer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long l) {
                NumberFormat f = new DecimalFormat("00");
                long min = (l / 60000) % 60;
                long sec = (l / 1000) % 60;
                T.setText(f.format(min) + ":" + f.format(sec));
            }

            @Override
            public void onFinish() {
                if (mRemainingQuestions != 0) {
                    mRemainingQuestions--;
                    Toast.makeText(getApplicationContext(), "Time's up", Toast.LENGTH_SHORT).show();
                    displayQuestion(generateQuestions().getNextQuestion());
                    countDownTimer.start();
                } else {
                    AlertDialog.Builder builder=new AlertDialog.Builder(GameActivity.this);
                    builder.setTitle("Well Done!")
                            .setMessage("Your score is "+Score)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                }
                            }).create().show();
                    countDownTimer.cancel();
                }
            }
        }.start();
        Hint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countDownTimer.cancel();
                Intent i = new Intent (getApplicationContext(), HintActivity.class);
                i.putExtra("url",mCurrentQuestion.getUrl());
                startActivity(i);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        countDownTimer.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();
    }

    private QuestionBank generateQuestions(){
        Question question1 =new Question("who's the 45th president of united states ?",
                Arrays.asList("Donald Trump", "Joe Biden", "Barack Obama",
                        "Paul Smith"),
                0, "https://fr.wikipedia.org/wiki/Liste_des_pr%C3%A9sidents_des_%C3%89tats-Unis");
        Question question2 =new Question("When did the first man land on the moon?",
                Arrays.asList("1958", "1962", "1967", "1969"),
                3,"https://en.wikipedia.org/wiki/Apollo_11");
        Question question3 =new Question("of what nationality is Shakespeare ?",
                Arrays.asList("French", "Italian", "Greek", "English"),
                3, "https://en.wikipedia.org/wiki/William_Shakespeare");
        Question question4 =new Question("How many people are in the world ?",Arrays.asList("7.9","6.8","8.2","7"),0,"https://en.wikipedia.org/wiki/World_population");

        return new QuestionBank(Arrays.asList(question1,question2,question3,question4));
    }
    private void displayQuestion (final Question question){
        questionText.setText(question.getText());
        Answer1.setText(question.getAnswers().get(0));
        Answer2.setText(question.getAnswers().get(1));
        Answer3.setText(question.getAnswers().get(2));
        Answer4.setText(question.getAnswers().get(3));
    }

    @Override
    public void onClick(View view) {
        Button b=(Button) view;
        countDownTimer.start();

        int index=0 ;
        if(b == Answer1){
            displayQuestion(generateQuestions().getNextQuestion());
        }
        else if (b == Answer2){
            index =1;
            displayQuestion(generateQuestions().getNextQuestion());
        }
        else if (b == Answer3){
            index =2;
            displayQuestion(generateQuestions().getNextQuestion());
        }
        else if (b == Answer4){
            index = 3;
            displayQuestion(generateQuestions().getNextQuestion());
        }
        else {
            try {
                throw new IllegalAccessException("Unknown clicked view"+ view);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        if ( index == mQuestionBank.getCurrentQuestion().getAnswerIndex()){
            Toast.makeText(this, "Correct", Toast.LENGTH_SHORT).show();
            Score++;
        }
        else {
            Toast.makeText(this, "Incorrect", Toast.LENGTH_SHORT).show();
        }
        mRemainingQuestions--;
        if (mRemainingQuestions>0){
            mCurrentQuestion=mQuestionBank.getNextQuestion();
            displayQuestion(mCurrentQuestion);
        }
        else{
            User mUser = new User(name, Score);
            mPlayersOperations.open();
            mPlayersOperations.ajouterPlayer(mUser);
            mPlayersOperations.close();
            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setTitle("Well Done!")
                    .setMessage("Your score is "+Score)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            Intent intent =new Intent(getApplicationContext(), ScoreActivity.class);
                            startActivity(intent);
                            //intent.putExtra(Bundle_extra_score, Score);
                            //setResult(RESULT_OK, intent);
                            //finish();

                        }
                    }).create().show();
            countDownTimer.cancel();

        }
    }
}